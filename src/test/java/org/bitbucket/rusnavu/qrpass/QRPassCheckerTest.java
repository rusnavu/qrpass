package org.bitbucket.rusnavu.qrpass;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import org.junit.*;

public class QRPassCheckerTest extends QRPassChecker {

	private static Map<BigInteger, Signature> signatures = new HashMap<>();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		put(signatures, (X509Certificate) QRPassTests.loadCertificate("/test.crt"));
		put(signatures, (X509Certificate) QRPassTests.loadCertificate("/root.crt"));
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		setSignatures(signatures);
	}

	private static void put(Map<BigInteger, Signature> map, X509Certificate certificate)
			throws NoSuchAlgorithmException, InvalidKeyException {
		Signature signature = Signature.getInstance(QRPassTests.ALGORITHM);
		signature.initVerify(certificate);
		map.put(certificate.getSerialNumber(), signature);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCrt() throws Exception {
		assertTrue(verify("Name: Иванов Иван Иванович\n" + 
				"Document: паспорт 4507 123456\n" + 
				"Crt: 00DF05C1337A5AFBCD\n" + 
				"Sig: 4979F6707FA4C55D6B39FFF97CC6567E070107E1C8D968797841D43A1F0DD371381A960EB8DBD7D0DE5060D8E45D71283387C919CAB3DD64ACD9EE669B3F6D7F"));
	}

	@Test
	public void testNoCrt() throws Exception {
		assertTrue(verify("Name: Иванов Иван Иванович\n" + 
				"Document: паспорт 4507 123456\n" + 
				"Sig: 9FADD99D3070D3351EF8BAD92D28D4B27A893E95CEA3AD2517DA57A40F84610BD34BCF30F4D168A2FA7A230680BC5B1D18C317F881A2EDCAAC3CD50D4D2E0BB8"));
	}

	@Test
	public void testFail() throws Exception {
		assertFalse(verify("Name: Иванов Иван Иванович\n" + 
				"Document: паспорт 4507 123457\n" + 
				"Sig: 9FADD99D3070D3351EF8BAD92D28D4B27A893E95CEA3AD2517DA57A40F84610BD34BCF30F4D168A2FA7A230680BC5B1D18C317F881A2EDCAAC3CD50D4D2E0BB8"));
	}
}

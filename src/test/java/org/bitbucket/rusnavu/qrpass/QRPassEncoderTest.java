package org.bitbucket.rusnavu.qrpass;

import static org.junit.Assert.*;

import java.security.PrivateKey;
import java.security.Signature;
import java.util.LinkedHashMap;

import org.bitbucket.rusnavu.qrpass.QRPassEncoder;
import org.junit.*;

public class QRPassEncoderTest extends QRPassEncoder {

	private static final String PRIVATE_KEY_RESOURCE = "/private.pem";
	private static PrivateKey privateKey;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		privateKey = QRPassTests.loadPrivateKey(PRIVATE_KEY_RESOURCE);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		Signature signature = Signature.getInstance(QRPassTests.ALGORITHM);
		signature.initSign(privateKey);
		setSignature(signature);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEncode() {
		LinkedHashMap<String, String> data = new LinkedHashMap<>();
		data.put("Name", "Иванов Иван Иванович");
		data.put("Document", "паспорт 4507 123456");
		data.put("Crt", "00DF05C1337A5AFBCD");
		String encoded = encode(data);
		assertTrue(encoded.startsWith("Name: Иванов Иван Иванович\n"
				+ "Document: паспорт 4507 123456"));
		assertTrue(encoded.matches("(?m)(.+\\n)+Sig:\\s[0-9A-F]+"));
	}

}

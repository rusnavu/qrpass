package org.bitbucket.rusnavu.qrpass;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.Signature;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class QRPassChecker extends QRBase {

	private static final Pattern PART = Pattern.compile("([^:]+):\\s+(.+)");
	private HashMap<BigInteger, Signature> signatures;

	public void setSignatures(Map<BigInteger, Signature> map) {
		signatures = new HashMap<>(map);
	}

	public boolean verify(String pass) throws SignatureException {
		String[] parts = pass.trim().split("\\s*\\n+\\s*");
		LinkedHashMap<String, String> data = new LinkedHashMap<>();
		for (String part : parts) {
			Matcher matcher = PART.matcher(part);
			if (matcher.matches())
				data.put(matcher.group(1).trim(), matcher.group(2).trim());
			else
				throw new IllegalArgumentException(part);
		}
		byte[] bs = fromHex(data.remove("Sig"));
		String string = encode(new StringBuilder(), data.entrySet().iterator()).toString();
		return verify(string, bs, data.get("Crt"));
	}

	private boolean verify(String string, byte[] sig, String serialNumber) {
		try {
			byte[] data = string.getBytes("UTF8");
			if (serialNumber != null) {
				Signature signature = getSignature(serialNumber);
				signature.update(data);
				return signature.verify(sig);
			} else {
				for (Signature signature : signatures.values()) {
					signature.update(data);
					try {
						if (signature.verify(sig))
							return true;
					} catch (SignatureException e) {
					}
				}
				return false;
			}
		} catch (UnsupportedEncodingException | SignatureException e) {
			throw new IllegalStateException(e);
		}
	}

	private Signature getSignature(String serialNumber) {
		BigInteger key = new BigInteger(fromHex(serialNumber));
		Signature signature = signatures.get(key);
		if (signature != null)
			return signature;
		throw new NoSuchElementException();
	}
}

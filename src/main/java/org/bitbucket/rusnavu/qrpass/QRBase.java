package org.bitbucket.rusnavu.qrpass;

import java.util.Iterator;
import java.util.Map.Entry;

public class QRBase {

	protected static StringBuilder encode(StringBuilder builder, Iterator<Entry<String, String>> it) {
		if (!it.hasNext())
			throw new IllegalArgumentException();
		append(builder, it.next());
		while (it.hasNext())
			append(builder.append('\n'), it.next());
		return builder;
	}

	private static void append(StringBuilder builder, Entry<String, String> entry) {
		builder.append(entry.getKey().trim())
				.append(": ")
				.append(entry.getValue().trim());
	}

	public static byte[] fromHex(String string) {
		string = string.replaceAll("\\s+", "");
		int length = string.length();
		if ((length % 2) != 0)
			throw new IllegalArgumentException();
		byte[] bs = new byte[length/2];
		for (int i = 0, from = 0, to = 2; from < length; from = to, to += 2)
			bs[i++] = (byte) Integer.parseUnsignedInt(string.substring(from, to), 0x10);
		return bs;
	}

	public static String toHex(byte[] bs) {
		StringBuilder out = new StringBuilder(bs.length*2);
		for (byte b : bs)
			out.append(String.format("%02X", b));
		return out.toString();
	}

}
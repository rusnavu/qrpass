package org.bitbucket.rusnavu.qrpass;

import java.awt.image.BufferedImage;
import java.security.Signature;
import java.util.Map;

import io.nayuki.qrcodegen.QrCode;

public class QRPassEncoder extends QRBase {

	private Signature signature;

	public void setSignature(Signature signature) {
		if (signature == null)
			throw new IllegalArgumentException();
		this.signature = signature;
	}

	public String encode(Map<String, String> data) {
		StringBuilder out = encode(new StringBuilder(), data.entrySet().iterator());
		byte[] signature = sign(out.toString());
		return out.append("\nSig: ").append(toHex(signature)).toString();
	}

	public BufferedImage encodeImage(Map<String, String> data) {
		return encodeImage(data, 10, 4);
	}

	public BufferedImage encodeImage(Map<String, String> data, int scale, int border) {
		String string = encode(data);
		QrCode qrCode = QrCode.encodeText(string, QrCode.Ecc.MEDIUM);
		return qrCode.toImage(scale, border);
	}

	private byte[] sign(String string) {
		if (signature == null)
			throw new IllegalStateException("No signature");
		try {
			byte[] bs = string.getBytes("UTF8");
			signature.update(bs);
			return signature.sign();
		} catch (Throwable e) {
			throw new IllegalStateException(e);
		}
	}
}
